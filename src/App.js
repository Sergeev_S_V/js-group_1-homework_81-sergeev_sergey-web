import React, { Component } from 'react';
import './App.css';
import {shortenUrl} from "./store/actions";
import {connect} from "react-redux";

const apiUrl = `http://localhost:8000/`;

class App extends Component {

  state = {
    baseUrl: '',
  };

  inputChangeHandler = event =>
    this.setState({[event.target.name]: event.target.value});

  render() {
    return (
      <div className="App">
        <h1>Shorter your link</h1>
        <input type="url" name='baseUrl'
               value={this.state.baseUrl}
               onChange={event => this.inputChangeHandler(event)}
        />
        <button onClick={() => this.props.onShortenUrl(this.state.baseUrl)}>
          Shorten!
        </button>
        {
          this.props.shortUrl
            ? <div>
                Your link looking like this:
                <a style={{pointerEvents: 'none'}} href="">
                  {apiUrl + this.props.shortUrl}
                </a>
              </div>
            : null
        }
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    shortUrl: state.shortUrl,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onShortenUrl: baseUrl => dispatch(shortenUrl(baseUrl)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
