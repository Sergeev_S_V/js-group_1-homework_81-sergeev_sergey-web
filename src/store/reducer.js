import {SUCCESS_SHORTENED_URL} from "./actionTypes";

const initialState = {
  shortUrl: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SUCCESS_SHORTENED_URL:
      return {shortUrl: action.shortUrl};
    default:
      return state;
  }
};

export default reducer;