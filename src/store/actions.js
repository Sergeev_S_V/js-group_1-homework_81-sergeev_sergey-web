import axios from 'axios';
import {SUCCESS_SHORTENED_URL} from "./actionTypes";

const apiUrl = `http://localhost:8000`;

export const shortenUrl = baseUrl => dispatch => {
  axios.post(`${apiUrl}`, {baseUrl})
    .then(res => {
      const shortUrl = res.data.shortUrl;
      dispatch(successShortenedUrl(shortUrl));
    });
};

export const successShortenedUrl = shortUrl => {
  return {type: SUCCESS_SHORTENED_URL, shortUrl};
};